const checkTypes = require('check-types')
const _ = require('lodash')
const methodsBySourceType = require('./methodsBySourceType.js')
const methods = require('./methods.js')
const Promise = require('bluebird')
const EventsEmitter = require('events')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Some arguments are expected.')

    if(!_.isObject(args.options))
        args.options = {}

    if(!(args.eventsEmitter instanceof EventsEmitter))
        throw new Error('Invalid eventsEmitter')

    checkTypes.assert.string(args.sourceType)
    
    if(_.isUndefined(methodsBySourceType[args.sourceType]))
        throw new Error('Unsopported source type.')
    
    checkTypes.assert.array(args.sources)
    return new Promise(function (resolve, reject) {
        var completeSources = {},
        sourcesReference = {},
        rejected = false

        _.forEach(args.sources, function(v, k){
            sourcesReference[v] = true
            methods[methodsBySourceType[args.sourceType]](v, args.options)
            .catch(function(error){
                args.eventsEmitter.emit('fail', {
                    sources: sourcesReference,
                    source: v,
                    loadedSources: completeSources,
                    pendingSources: _.omit(sourcesReference, _.keys(completeSources)) 
                })
                rejected = true
                return reject(new Error('Error while loading source ' + v))
            })
            .then(function(){
                if(args.stopOnReject === true && rejected === true)
                    return false
                
                completeSources[v] = true
                args.eventsEmitter.emit('progress', {
                    sources: sourcesReference,
                    source: v,
                    loadedSources: completeSources,
                    pendingSources: _.omit(sourcesReference, _.keys(completeSources)) 
                })

                if(_.size(completeSources) === _.size(sourcesReference))
                    resolve(completeSources)
            })
        })
    })
}