const checkTypes = require('check-types')
const imagePromise = require('image-promise')

module.exports = function(source){
    checkTypes.assert.string(source)
    return imagePromise(source)
}